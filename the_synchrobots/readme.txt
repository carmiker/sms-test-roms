*******************
* The synchrobots *
*******************

Code & music by GliGli, GFX by Ced2911.

For VIP 2017 demo party ( http://vip2017.popsyteam.org/ ).

Plays on a 50Hz Sega Master System with its 3.5Mhz Z80.
Most emulators should be able to play it perfectly (Emulicious, MEKA, Kega Fusion, ...). Make sure to set them to PAL 50Hz.
